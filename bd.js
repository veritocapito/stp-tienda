var Promise = require('promise');
var mysql      = require('mysql');
var util = require('util');

var pool = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'vero8639',
    database : 'sisterstp',
    port:3306
});
  
pool.query = util.promisify(pool.query);

module.exports = pool;