var express = require('express');
var router = express.Router();
var pool = require('../bd');

var  multer   = require('multer') 
var  upload  = multer({dest:'./uploads/'} ) 

var fs = require('fs');

/* GET products listing. */

router.get('/list/', async function(req, res, next) {

  let products = await pool.query("select * from product");
  res.render('products', {data: products});
});



router.get('/create', async function(req, res, next) {

    res.render('create');
});

router.post('/create', async function(req, res, next) {


    let sentencia = `    
    insert into product(name, description, presentation, unit) values ('${req.body.name}','${req.body.description}','${req.body.presentation}','${req.body.unit}')`;     

    console.log(sentencia);

    let resultado = await pool.query(sentencia);


    res.render('done',{mensaje:"Producto Ingresado Exitosamente"});
});


router.get('/update/:id', async function(req, res, next) {

  let products = await pool.query("select * from product where id = " + req.params.id);

  res.render('update',{data:products});
});


router.post('/update/:id', async function(req, res, next) {

  let sentencia;

  if(req.file){

      sentencia = `update products
      set name  = '${req.body.name}',  
      description  = '${req.body.description}',
      presentation  = '${req.body.presentation}',
      unit  = '${req.body.unit}',
      imagen = '/images/${req.file.originalname}' 
       where id = ${req.params.id} 
      `

      fs.createReadStream('./uploads/'+req.file.filename).pipe(fs.createWriteStream('./public/images/'+req.file.originalname, function(error){}));
      //borramos el archivo temporal creado

      fs.unlink('./uploads/'+req.file.filename, function(error){
        console.log("No lo puedo borraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaarrrrrrrrrrrrrrrrrrrrrrrrrr " + error);

      });

  } else{
      sentencia = `update products
      set name  = '${req.body.name}',  
      description  = '${req.body.description}',
      presentation  = '${req.body.presentation}',
      unit  = '${req.body.unit}',
       where id = ${req.params.id} 
      `
  }

  res.render('done',{mensaje:"El Producto fue modificado exitosamente!"});

});


router.get('/delete/:id', async function(req, res, next) {

  let products = await pool.query("select * from product where id = " + req.params.id);

  res.render('delete',{data:products});
});


router.post('/delete/:id', async function(req, res, next) {

    await pool.query("delete from product where id = " + req.params.id);

    res.render('done',{mensaje:"El producto fue eliminado exitosamente!"});

});



module.exports = router;