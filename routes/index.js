var express = require('express');
var router = express.Router();

let mail = require('../mail');

/* GET home page. */
router.get('/', async function(req, res, next) {

  res.render('index', { title: 'Sisters - Talles Positivos' });
});

router.get('/contacto/', async function(req, res, next) {

  res.render('contacto', {});
});

router.post('/contacto/', async function(req, res, next) {


  let texto = `Nombre: ${req.body.nombre} \nApellido: ${req.body.apellido} \nCorreo: ${req.body.email}`;

  let info = await mail.main(texto);

   res.render('mensajeMail',{mensaje:"Mail enviado correctamente"});

});

module.exports = router;


